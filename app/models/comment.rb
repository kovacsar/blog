class Comment < ActiveRecord::Base
  # other side of the one-to many relation with Post
  belongs_to :post
  
  # basic validations for comment fields
  validates :post_id, presence: true
  validates :body, presence: true
end

class Post < ActiveRecord::Base
  # making one-to many relation with comments, and if post is destroyed, all of its comments will be destroyed too
  has_many :comments, dependent: :destroy
  
  # basic validations for post fields
  validates_presence_of :title
  validates_presence_of :body
end
